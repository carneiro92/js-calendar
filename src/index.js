import "./scss/index.scss";
import moment from 'moment';
import { Events } from "./events";
//boucle event.start = moment(event.start)

let events = JSON.parse(localStorage.getItem("events"));
if (events === null) {
    events = [];
}
var storedEvents = events;
console.log(storedEvents);
let todaye;
let today = moment();
var todayM = moment([2019, 1, 1]);
var currentMonth = todayM.month();
var currentYear = todayM.year();
var selected = null;

let selectYear = document.getElementById("year");
let selectMonth = document.getElementById("month");
let prevB = document.getElementById('previous');
let nextB = document.getElementById('next');
selectMonth.onchange = function () { jump(); };
selectYear.onchange = function () { jump(); };


let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

let monthAndYear = document.getElementById("monthAndYear");
showCalendar(currentMonth, currentYear);
prevB.addEventListener('click', function previous() {
    currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
    currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
    showCalendar(currentMonth, currentYear);
});
nextB.addEventListener('click', function next() {
    currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
    currentMonth = (currentMonth + 1) % 12;
    showCalendar(currentMonth, currentYear);
});

function jump() {
    currentYear = parseInt(selectYear.value);
    currentMonth = parseInt(selectMonth.value);
    showCalendar(currentMonth, currentYear);
}
function showCalendar(month, year) {
    let firstDay = moment([year, month]).weekday();
    let tbl = document.getElementById("calendar-body"); // body of the calendar

    // clearing all previous cells
    tbl.innerHTML = "";

    // filing data about month and in the page via DOM.
    monthAndYear.innerHTML = months[month] + " " + year;
    selectYear.value = year;
    selectMonth.value = month;

    // creating all cells
    let date = 1;
    for (let i = 0; i < 6; i++) {
        // creates a table row
        let row = document.createElement("tr");

        //creating individual cells, filing them up with data.
        for (let j = 0; j < 7; j++) {
            if (i === 0 && j < firstDay) {
                let cell = document.createElement("td");
                let cellText = document.createTextNode("");
                cell.appendChild(cellText);
                row.appendChild(cell);
            }
            else if (date > moment([year, month]).daysInMonth()) {
                break;
            }

            else {
                let cell = document.createElement("td");
                let cellText = document.createTextNode(date);
                if (date === today.date() && year === today.year() && month === today.month()) {
                    cell.classList.add("bg-info");
                } // color today's date
                cell.id = date;
                cell.onclick = function () { listEvents(parseInt(cell.id)); selected = null; };
                cell.appendChild(cellText);
                row.appendChild(cell);
                date++;
            }


        }

        tbl.appendChild(row); // appending each row into calendar body.
    }
    
    updateSelect();
}
/**
 * 
 * @param {number} day
 *  
 */
function eventChange(event) {
    //todaye = moment().date(day).month(currentMonth).year(currentYear);
    document.getElementById('start').value = event.start.format('YYYY-MM-DD');
    document.getElementById('eventName').value = event.title;
    document.getElementById('eventDescription').value = event.description;
    document.getElementById('eventLocation').value = event.location;
    selected = event;
}

function listEvents(day) {
    let todaye = moment().date(day).month(currentMonth).year(currentYear);
    document.getElementById('start').value = todaye.format('YYYY-MM-DD');
    let listEvents = document.getElementById('event');
    listEvents.innerHTML = "";
    for (let index = 0; index < storedEvents.length; index++) {
        if (storedEvents[index].start.month() === currentMonth) {
            let date = document.getElementById(storedEvents[index].start.date());
            date.classList.add("bg-info");
        }

        if (storedEvents[index].start.isSame(todaye, 'day')) {
            let divEvent= document.createElement('div');
            let divEventCol= document.createElement('div');
            let todayEvent = document.createElement('p');
            todayEvent.innerHTML = storedEvents[index].title +", "+ storedEvents[index].description +" à "+ storedEvents[index].location;
            todayEvent.className='bg-info border border-primary rounded';
            divEvent.className='row-6';
            divEventCol.className='col-12';
            todayEvent.onclick = function () { eventChange(storedEvents[index]); };
            listEvents.appendChild(divEvent);
            divEvent.appendChild(divEventCol);
            divEventCol.appendChild(todayEvent);

        }
    }
    updateEvents();
}

let form = document.getElementById('formE');
form.addEventListener('submit', function (event) {
    event.preventDefault();
    let name = document.getElementById('eventName').value;
    let eventDesc = document.getElementById('eventDescription').value;
    let eventLocation = document.getElementById('eventLocation').value;
    let start = moment(document.getElementById('start').value);
    //let startTime = moment(document.getElementById('startTime').value, 'HH:mm');
    //let endTime = moment(document.getElementById('endTime').value, 'HH:mm');
    //start.startOf(startTime.hour(), startTime.minutes());
    //start.endOf(endTime.hour(), endTime.minutes());
    let eventN = new Events(name, eventDesc, eventLocation, start);
    if (selected === null) {
        events.push(eventN);
        console.log('bloup');
    } else {
        
        selected.title = name;
        selected.description = eventDesc;
        selected.location = eventLocation;
        selected.start = start;
        localStorage.setItem("events", JSON.stringify(events));
        event.preventDefault();

        let todayEvent = document.createElement('p');
        //todayEvent.innerHTML = name +", "+ description +" à "+ location;
        todayEvent.innerHTML = storedEvents[index].title +", "+ storedEvents[index].description +" à "+ storedEvents[index].location;

    }

    selected = null;
});
function updateEvents() {
    for (const i of events) {
        i.start = moment(i.start);
        let date = document.getElementById(i.start.date());
        if (i.start.month() === currentMonth) {
            date.classList.add("bg-info");
        }
        else{
            date.className="";
        }
    }
}
function updateSelect() {
    setInterval(function () {
        let modify = document.getElementById('modifyEvent');
        let create = document.getElementById('createEvent');
        let delet = document.getElementById('deleteEvent');
        updateEvents();
        if (selected !== null) {
            modify.className = 'btn btn-primary ml-4 mr-3 d-block';
            delet.className = 'btn btn-primary ml-4 mr-3 d-block';
            create.className = 'btn btn-primary ml-4 mr-3 d-none';
        } else if (selected === null) {
            create.className = 'btn btn-primary ml-4 mr-3 d-block';
            modify.className = 'btn btn-primary ml-4 mr-3 d-none';
            delet.className = 'btn btn-primary ml-4 mr-3 d-none';
            //document.getElementById('eventName').value = '';
            //document.getElementById('eventDescription').value = '';
            //document.getElementById('eventLocation').value = '';
        }
        console.log(selected.title);
    }, 1000);
}

let delet = document.getElementById('deleteEvent');
delet.addEventListener('click', function(){
    events.splice(storedEvents.indexOf(selected.title));
    localStorage.setItem("events", JSON.stringify(events));
    alert('This appointment has been deleted.');
    location.reload();
})